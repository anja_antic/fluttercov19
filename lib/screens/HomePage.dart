import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterCovid19/utils/Dimensions.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.grey,
      appBar: new AppBar(
        toolbarHeight: screenHeight(context, dividedBy: 8),
        backgroundColor: Colors.white,
      ),
      body: new Center(
        child: new Text(
          "This is Home Page",
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
