import 'package:flutter/material.dart';
import 'package:flutterCovid19/utils/Dimensions.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.lightBlue,
      appBar: new AppBar(
        toolbarHeight: screenHeight(context, dividedBy: 8),
        backgroundColor: Colors.white,
      ),
      body: new Center(
        child: new Text(
          "This is Notifications Page",
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
