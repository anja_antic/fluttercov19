import 'package:flutter/material.dart';
import 'package:flutterCovid19/utils/Dimensions.dart';

class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.lightGreen,
      appBar: new AppBar(
        toolbarHeight: screenHeight(context, dividedBy: 8),
        backgroundColor: Colors.white,
      ),
      body: new Center(
        child: new Text(
          "This is News Page",
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
