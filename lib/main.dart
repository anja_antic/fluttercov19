import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterCovid19/screens/HomePage.dart';
import 'package:flutterCovid19/screens/NewsPage.dart';
import 'package:flutterCovid19/screens/NotificationsPage.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarBrightness: Brightness.light,
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(home: BottomNavBar());
  }
}

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int currentTabIndex = 0;
  final List<Widget> children = [HomePage(), NewsPage(), NotificationsPage()];

  void onTabClicked(int index) {
    setState(() {
      currentTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: children[currentTabIndex],
      bottomNavigationBar: Container(
          padding: EdgeInsets.only(top: 20),
          child: BottomNavigationBar(
            elevation: 0,
            onTap: onTabClicked,
            currentIndex: currentTabIndex,
            items: [
              BottomNavigationBarItem(icon: new Icon(Icons.home), label: ""),
              BottomNavigationBarItem(icon: new Icon(Icons.list), label: ""),
              BottomNavigationBarItem(
                  icon: new Icon(Icons.notifications), label: "")
            ],
          )),
    );
  }
}
